import 'package:flutter/material.dart';

import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';

import './account_screen.dart';

class ChangePasswordScreen extends StatefulWidget {
  static const routeName = '/change_password';

  ChangePasswordScreen({Key key}) : super(key: key);

  @override
  _ChangePasswordScreenState createState() => _ChangePasswordScreenState();
}

class _ChangePasswordScreenState extends State<ChangePasswordScreen> {
  bool obscurePassword = true;

  final GlobalKey<FormBuilderState> _fbKey = GlobalKey<FormBuilderState>();

  @override
  Widget build(BuildContext context) {
    void goBack() {
      Navigator.of(context).pushReplacementNamed(AccountScreen.routeName);
    }

    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(Icons.arrow_back_ios),
          onPressed: goBack,
        ),
        backgroundColor: Color.fromRGBO(207, 39, 44, 1),
        centerTitle: true,
        title: Text(
          'Change Password',
          style: TextStyle(
            fontFamily: 'Encode Sans',
            fontSize: 18.sp,
            fontWeight: FontWeight.w500,
          ),
        ),
      ),
      body: Container(
        margin: EdgeInsets.symmetric(horizontal: 24.w),
        child: SingleChildScrollView(
          child: FormBuilder(
            key: _fbKey,
            child: Column(
              children: [
                SizedBox(
                  height: 37.h,
                ),
                buildContainer('Old password'),
                SizedBox(
                  height: 36.h,
                ),
                buildContainer('New password'),
                SizedBox(
                  height: 36.h,
                ),
                buildContainer('Repeat new password'),
                SizedBox(
                  height: 257.h,
                ),
                Container(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Container(
                        width: 150.w,
                        height: 40.h,
                        child: FlatButton(
                          shape: RoundedRectangleBorder(
                            side: BorderSide(
                                color: Color.fromRGBO(207, 39, 44, 1),
                                width: 1),
                            borderRadius: BorderRadius.all(
                              Radius.circular(5),
                            ),
                          ),
                          onPressed: goBack,
                          child: Text(
                            'Cancel',
                            style: TextStyle(
                              color: Color.fromRGBO(207, 39, 44, 1),
                              fontFamily: 'Encode Sans',
                              fontSize: 18.sp,
                              fontWeight: FontWeight.w500,
                            ),
                          ),
                        ),
                      ),
                      Container(
                        width: 150.w,
                        height: 40.h,
                        child: FlatButton(
                          shape: RoundedRectangleBorder(
                            side: BorderSide(
                                color: Color.fromRGBO(207, 39, 44, 1),
                                width: 1),
                            borderRadius: BorderRadius.all(
                              Radius.circular(5),
                            ),
                          ),
                          color: Color.fromRGBO(207, 39, 44, 1),
                          onPressed: goBack,
                          child: Text(
                            'Save',
                            style: TextStyle(
                                color: Colors.white,
                                fontFamily: 'Encode Sans',
                                fontSize: 18.sp,
                                fontWeight: FontWeight.w500),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Container buildContainer(String label) {
    return Container(
      height: 45.h,
      child: FormBuilderTextField(
        attribute: 'textfield',
        obscureText: obscurePassword,
        decoration: InputDecoration(
          suffix: Container(
            margin: EdgeInsets.only(
              right: 5.w,
            ),
            child: InkWell(
              onTap: () {
                setState(() {
                  obscurePassword = !obscurePassword;
                });
              },
              child: Image.asset(
                'assets/images/eye.png',
                width: 24.w,
              ),
            ),
          ),
          labelText: label,
          labelStyle: TextStyle(
            color: Color.fromRGBO(126, 131, 142, 1),
            fontFamily: 'Encode Sans',
            fontWeight: FontWeight.w400,
            fontSize: 12.sp,
          ),
          border: OutlineInputBorder(
            borderSide: BorderSide(),
          ),
          focusedBorder: OutlineInputBorder(
            borderSide: BorderSide(
              color: Color.fromRGBO(222, 219, 230, 1),
            ),
          ),
        ),
      ),
    );
  }
}
