import 'package:flutter/material.dart';

import 'package:flutter_screenutil/flutter_screenutil.dart';

class ChangeName extends StatefulWidget {
  @override
  _ChangeNameState createState() => _ChangeNameState();
}

class _ChangeNameState extends State<ChangeName> {
  bool _chName = false;
  var _nameController = TextEditingController();
  var _name = 'Darian Gipich';

  _changingName() {
    setState(() {
      _chName = !_chName;
      if (_nameController.text != null && _nameController.text != '') {
        _name = _nameController.text;
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [
          Container(
            width: double.infinity,
            child: Text(
              'Full name',
              style: TextStyle(
                fontFamily: 'Encode Sans',
                fontWeight: FontWeight.w400,
                fontSize: 12.sp,
                color: Color.fromRGBO(126, 131, 142, 1),
              ),
            ),
          ),
          SizedBox(
            height: 7.h,
          ),
          // changingName == false
          // ?
          _chName == false
              ? Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      _name,
                      style: TextStyle(
                        fontFamily: 'Encode Sans',
                        fontWeight: FontWeight.w400,
                        fontSize: 16.sp,
                      ),
                    ),
                    InkWell(
                      onTap: _changingName,
                      child: Image.asset(
                        'assets/images/pencil.png',
                        width: 16.w,
                        height: 16.w,
                      ),
                    ),
                  ],
                )
              : TextField(
                  onEditingComplete: _changingName,
                  decoration: InputDecoration(
                    hintText: _name,
                    border: OutlineInputBorder(),
                  ),
                  controller: _nameController,
                ),
        ],
      ),
    );
  }
}
