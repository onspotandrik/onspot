import 'package:flutter/material.dart';

import 'package:flutter_screenutil/flutter_screenutil.dart';

import './account_screen.dart';

class Inspections extends StatelessWidget {
  final Function goToAccountScreen;

  Inspections(this.goToAccountScreen);

  @override
  Widget build(BuildContext context) {
    void goToAccount() {
      Navigator.of(context).pushReplacementNamed(AccountScreen.routeName);
    }

    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color.fromRGBO(207, 39, 44, 1),
        leading: Row(
          children: [
            SizedBox(
              width: 24.w,
            ),
            // Container(
            //   // margin: EdgeInsets.only(left: 31.h, top: 20.h, bottom: 10.h),
            //   padding: EdgeInsets.all(7.w),
            //   decoration: BoxDecoration(
            //     borderRadius: BorderRadius.circular(40.h),
            //     color: Colors.white,
            //   ),
            //   // height: 10.h,
            //   // width: 10.h,
            //   child: InkWell(
            //     onTap: goToAccount,
            //     child: Image.asset(
            //       'assets/images/person24px.png',
            //       width: 16.w,
            //     ),
            //   ),
            // ),
            CircleAvatar(
              radius: 15.h,
              backgroundColor: Colors.white,
              child: Container(
                margin: EdgeInsets.symmetric(horizontal: 8.w),
                child: InkWell(
                  onTap: goToAccount,
                  child: Image.asset(
                    'assets/images/person24px.png',
                  ),
                ),
              ),
            ),
          ],
        ),
        leadingWidth: 70.w,
        title: Text(
          'Inspections',
          style: TextStyle(
            fontFamily: 'Encode Sans',
            fontWeight: FontWeight.w500,
            fontSize: 18.h,
          ),
        ),
        centerTitle: true,
        actions: [
          GestureDetector(
            onTap: () {},
            child: Image.asset(
              'assets/images/filter.png',
              width: 20.w,
              height: 20.w,
            ),
          ),
          SizedBox(
            width: 30.w,
          ),
        ],
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Container(
              child: Column(
                children: [
                  Container(
                    margin: EdgeInsets.symmetric(horizontal: 24.w),
                    child: Column(
                      children: [
                        SizedBox(
                          height: 20.h,
                        ),
                        Row(
                          crossAxisAlignment: CrossAxisAlignment.end,
                          children: [
                            Container(
                              // color: Colors.black54,
                              width: ((375 - 48) / 2).w,
                              child: Text(
                                'ADDRESS',
                                style: TextStyle(
                                  height: null,
                                  fontSize: 12.sp,
                                  fontFamily: 'Encode Sans',
                                  fontWeight: FontWeight.w500,
                                  color: Color.fromRGBO(126, 131, 142, 1),
                                ),
                              ),
                            ),
                            Container(
                              width: ((375 - 48) / 2).w,
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.end,
                                children: [
                                  Container(
                                    margin: EdgeInsets.only(right: 30.w),
                                    child: Text(
                                      'PAYMENT',
                                      style: TextStyle(
                                        fontSize: 12.sp,
                                        fontFamily: 'Encode Sans',
                                        fontWeight: FontWeight.w500,
                                        color: Color.fromRGBO(126, 131, 142, 1),
                                      ),
                                    ),
                                  ),
                                  Container(
                                    child: Text(
                                      'STATUS',
                                      style: TextStyle(
                                        fontSize: 12.sp,
                                        fontFamily: 'Encode Sans',
                                        fontWeight: FontWeight.w500,
                                        color: Color.fromRGBO(126, 131, 142, 1),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                        SizedBox(
                          height: 10.h,
                        ),
                      ],
                    ),
                  ),
                  Divider(),
                ],
              ),
            ),
            buildContainer(
              '345 Brown Ave',
              'assets/images/paid.png',
              'CLOSED',
              '10 Apr,2020',
            ),
            buildContainer(
              '345 Brown Ave',
              'assets/images/notPaid.png',
              'OPEN',
              '8 Apr,2020',
            ),
            buildContainer(
              '345 Brown Ave',
              'assets/images/notPaid.png',
              'IN PROGRESS',
              '5 Apr,2020',
            ),
            buildContainer(
              '13 Lyham Rd, Brixton, London SW 4',
              'assets/images/paid.png',
              'IN PROGRESS',
              '3 Apr,2020',
            ),
            buildContainer(
              '345 Brown Ave',
              'assets/images/paid.png',
              'IN PROGRESS',
              '3 Apr,2020',
            ),
            buildContainer(
              '345 Brown Ave',
              'assets/images/paid.png',
              'IN PROGRESS',
              '3 Apr,2020',
            ),
          ],
        ),
      ),
    );
  }

  Container buildContainer(
      String address, String img, String status, String date) {
    return Container(
      child: Column(
        children: [
          Container(
            margin: EdgeInsets.symmetric(horizontal: 24.w),
            child: Column(
              children: [
                SizedBox(
                  height: 20.h,
                ),
                Column(
                  children: [
                    Row(
                      children: [
                        Container(
                          width: ((375 - 48) / 2).w,
                          child: Text(
                            address,
                            style: TextStyle(
                              color: Color.fromRGBO(68, 67, 71, 1),
                              fontFamily: 'Encode Sans',
                              fontWeight: FontWeight.w500,
                              fontSize: 18.sp,
                            ),
                          ),
                        ),
                        Container(
                          width: ((375 - 48) / 2).w,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Container(
                                margin: EdgeInsets.only(left: 48.w),
                                child: Image.asset(
                                  img,
                                  width: 20.h,
                                ),
                              ),
                              status == 'CLOSED'
                                  ? Text(status,
                                      style: TextStyle(
                                        color: Color.fromRGBO(35, 159, 149, 1),
                                        fontFamily: 'Encode Sans',
                                        fontWeight: FontWeight.w600,
                                        fontSize: 12.sp,
                                      ))
                                  : status == 'OPEN'
                                      ? Text(
                                          status,
                                          style: TextStyle(
                                            color:
                                                Color.fromRGBO(235, 95, 93, 1),
                                            fontFamily: 'Encode Sans',
                                            fontWeight: FontWeight.w600,
                                            fontSize: 12.sp,
                                          ),
                                        )
                                      : Text(
                                          status,
                                          style: TextStyle(
                                            color: Color.fromRGBO(
                                                250, 188, 116, 1),
                                            fontFamily: 'Encode Sans',
                                            fontWeight: FontWeight.w600,
                                            fontSize: 12.sp,
                                          ),
                                        )
                            ],
                          ),
                        ),
                      ],
                    ),
                    Row(
                      children: [],
                    ),
                  ],
                ),
                SizedBox(
                  height: 10.h,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      'John Smith',
                      style: TextStyle(
                        color: Color.fromRGBO(68, 67, 71, 1),
                        fontFamily: 'Encode Sans',
                        fontWeight: FontWeight.w300,
                        fontSize: 16.sp,
                      ),
                    ),
                    Text(
                      date,
                      style: TextStyle(
                        color: Color.fromRGBO(126, 131, 142, 1),
                        fontFamily: 'Encode Sans',
                        fontWeight: FontWeight.w300,
                        fontSize: 12.sp,
                      ),
                    ),
                  ],
                ),
                SizedBox(
                  height: 20.h,
                ),
              ],
            ),
          ),
          Divider(),
        ],
      ),
    );
  }
}
